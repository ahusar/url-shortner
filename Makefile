install-project:
	make build
	make start
	make composer-install
	make init-db
	make migrate
	make load-fixtures
	make cache-clear

init-db:
	docker-compose exec sf_app bin/console doctrine:database:create --if-not-exists

migrate:
	docker-compose exec sf_app bin/console doctrine:migration:migrate -n

load-fixtures:
	docker-compose exec sf_app bin/console doctrine:fixtures:load -n

build:
	docker-compose build --no-cache

start:
	docker-compose up -d; \

stop:
	docker-compose down; \

composer-install:
	docker-compose exec sf_app composer install

composer:
	docker-compose exec sf_app composer $(filter-out $@,$(MAKECMDGOALS))

console:
	docker-compose exec sf_app bin/console $(filter-out $@,$(MAKECMDGOALS))

cache-clear:
	docker-compose exec sf_app bin/console cache:clear

connect:
	docker-compose exec sf_app bash

run-tests:
	docker-compose exec sf_app bin/phpunit $(filter-out $@,$(MAKECMDGOALS))