URL-Shortener
================

#### Installation steps

1. Clone the project:

```
git clone git@gitlab.com:ahusar/url-shortner.git
```

2. Copy env vars

```
cp .env.dist .env
```

2 - Run the project:

```
make install-project
```

3 - Access Example Page

```
http://localhost/index
```

4 - Run unit tests

```
make run-tests
```