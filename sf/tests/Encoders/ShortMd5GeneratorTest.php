<?php

namespace App\Tests\Encoders;

use App\Encoders\ShortMd5Generator;
use PHPUnit\Framework\TestCase;

class ShortMd5GeneratorTest extends TestCase
{
    /** @var ShortMd5Generator */
    private $subject;

    public function setUp(): void
    {
        $this->subject = new ShortMd5Generator();
    }

    public function testTokenGenerationDoNotMatch()
    {
        $token1 = $this->subject->generateToken();
        $token2 = $this->subject->generateToken();

        $this->assertNotEquals($token1, $token2);
    }
}
