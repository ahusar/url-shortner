<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Exception\EmailUsedException;
use App\Repository\UserRepository;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockClass;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserServiceTest extends TestCase
{
    /** @var MockClass|EntityManagerInterface */
    private $emMock;
    /** @var MockClass|UserRepository */
    private $userRepoMock;
    /** @var MockClass|UserPasswordEncoderInterface */
    private $passEncoderMock;
    /** @var MockClass|User */
    private $userMock;
    /** @var UserService */
    private $userService;

    public function setUp(): void
    {
        $this->emMock = $this->createMock(EntityManagerInterface::class);
        $this->userRepoMock = $this->createMock(UserRepository::class);
        $this->emMock->expects($this->any())->method('getRepository')->willReturn($this->userRepoMock);
        $this->passEncoderMock = $this->createMock(UserPasswordEncoderInterface::class);
        $this->userMock = $this->createMock(User::class);
        $this->userService = new UserService($this->emMock, $this->passEncoderMock);
    }

    public function testCreateUserException()
    {
        $this->userRepoMock->expects($this->once())->method('findOneBy')->willReturn($this->userMock);
        $this->expectException(EmailUsedException::class);
        $this->userService->createUser($this->getData());
    }

    public function testCreateUser()
    {
        $this->userRepoMock->expects($this->once())->method('findOneBy')->willReturn(null);
        $this->passEncoderMock->expects($this->once())->method('encodePassword')->willReturn('123');
        $result = $this->userService->createUser($this->getData());
        $this->assertInstanceOf(User::class, $result);
        $this->assertEquals($this->getData()['email'], $result->getEmail());
    }

    private function getData()
    {
        return ['email' => 'test', 'password' => '123'];
    }
}
