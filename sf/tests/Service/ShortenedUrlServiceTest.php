<?php

namespace App\Tests\Service;

use App\Encoders\TokenGeneratorInterface;
use App\Entity\ShortenedUrl;
use App\Entity\User;
use App\Service\ShortenedUrlService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockClass;
use PHPUnit\Framework\TestCase;

class ShortenedUrlServiceTest extends TestCase
{
    /** @var MockClass|EntityManagerInterface */
    private $emMock;
    /** @var MockClass|TokenGeneratorInterface */
    private $tokenGeneratorMock;
    /** @var MockClass|User */
    private $userMock;
    /** @var MockClass|ObjectRepository */
    private $repositoryMock;
    /** @var ShortenedUrlService */
    private $shortenedUrlService;

    public function setUp(): void
    {
        $this->emMock = $this->createMock(EntityManagerInterface::class);
        $this->tokenGeneratorMock = $this->createMock(TokenGeneratorInterface::class);
        $this->userMock = $this->createMock(User::class);
        $this->repositoryMock = $this->createMock(ObjectRepository::class);
        $this->emMock->expects($this->any())->method('getRepository')->willReturn($this->repositoryMock);

        $this->shortenedUrlService = new ShortenedUrlService($this->emMock, $this->tokenGeneratorMock);
    }

    public function testCreateUrl()
    {
        $result = $this->shortenedUrlService->createShortenedUrl($this->getData(), $this->userMock);
        $this->assertInstanceOf(ShortenedUrl::class, $result);
    }

    public function testGetAndVisitUrl()
    {
        $url = $this->shortenedUrlService->createShortenedUrl($this->getData(), $this->userMock);
        $this->repositoryMock->expects($this->once())->method('findOneBy')->willReturn($url);
        $this->assertEquals(0, $url->getUsages());
        $result = $this->shortenedUrlService->getAndVisitUrl($url->getToken());
        $this->assertEquals(1, $url->getUsages());
        $this->assertEquals($this->getData()['sourceUrl'], $result);
    }

    public function testListUrlsByUser()
    {
        $shortenedUrl = new ShortenedUrl();
        $shortenedUrl->setSourceUrl('test');
        $this->userMock->expects($this->once())->method('getShortenedUrls')->willReturn(
            new ArrayCollection([$shortenedUrl])
        );
        $result = $this->shortenedUrlService->listUrlsByUser($this->userMock);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('sourceUrl', $result[0]);
        $this->assertArrayHasKey('token', $result[0]);
        $this->assertArrayHasKey('usages', $result[0]);
    }

    public function testEditUrl()
    {
        $shortenedUrl = new ShortenedUrl();
        $shortenedUrl->setSourceUrl('test');
        $this->repositoryMock->expects($this->once())->method('findOneBy')->willReturn($shortenedUrl);
        $result = $this->shortenedUrlService->editUrl($this->userMock, $this->getData(), 0);
        $this->assertEquals($this->getData()['sourceUrl'], $result->getSourceUrl());
    }

    public function testExceptionDeleteUrl()
    {
        $this->repositoryMock->expects($this->once())->method('findOneBy')->willThrowException(
            new EntityNotFoundException()
        );
        $this->expectException(EntityNotFoundException::class);
        $this->shortenedUrlService->deleteUrl($this->userMock, 0);

    }

    private function getData(): array
    {
        return ['sourceUrl' => 'test.google.com'];
    }
}
