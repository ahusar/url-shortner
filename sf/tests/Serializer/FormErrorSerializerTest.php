<?php

namespace App\Tests\Serializer;

use App\Serializer\FormErrorSerializer;
use PHPUnit\Framework\MockObject\MockClass;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class FormErrorSerializerTest extends TestCase
{
    /** @var MockClass|FormInterface */
    private $formMock;
    /** @var MockClass|FormError */
    private $errorMock;
    /** @var FormErrorSerializer */
    private $errorSerializer;

    public function setUp(): void
    {
        $this->formMock = $this->createMock(FormInterface::class);
        $this->errorMock = $this->createMock(FormError::class);
        $this->errorSerializer = new FormErrorSerializer();
    }

    public function testNoErrorsReturned(): void
    {
        $this->formMock->expects($this->once())->method('getErrors')->willReturn([]);
        $returnedErrors = $this->errorSerializer->convertFormToArray($this->formMock);
        $this->assertEquals([], $returnedErrors);
    }

    public function testReturnedErrors(): void
    {
        $this->formMock->expects($this->once())->method('getErrors')->willReturn([$this->errorMock]);
        $this->errorMock->expects($this->once())->method('getOrigin')
            ->willReturn($this->formMock);
        $this->errorMock->expects($this->once())->method('getMessage')->willReturn('error at property');
        $this->formMock->expects($this->once())->method('getName')->willReturn('propertyName');

        $resultedErrors = $this->errorSerializer->convertFormToArray($this->formMock);
        $this->assertEquals([[['message' => 'error at property', 'field' => 'propertyName']]], $resultedErrors);
    }
}
