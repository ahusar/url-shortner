<?php

namespace App\Controller;

use App\Service\ShortenedUrlService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UrlAccessController extends AbstractController
{
    /**
     * @var ShortenedUrlService
     */
    private $urlService;

    /**
     * @param ShortenedUrlService $shortenedUrlService
     */
    public function __construct(ShortenedUrlService $shortenedUrlService)
    {
        $this->urlService = $shortenedUrlService;
    }

    /**
     * @Route("/{token}", name="url_access", methods={"GET"})
     *
     * @param string $token
     *
     * @return RedirectResponse
     * @throws NotFoundHttpException
     */
    public function index(string $token): RedirectResponse
    {
        try {
            $urlToVisit = $this->urlService->getAndVisitUrl($token);
        } catch (\Exception $ex) {
            throw $this->createNotFoundException();
        }

        return $this->redirect($urlToVisit);
    }

}
