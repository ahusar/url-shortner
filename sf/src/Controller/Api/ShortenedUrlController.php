<?php

namespace App\Controller\Api;

use App\Form\ShortenedUrlType;
use App\Serializer\FormErrorSerializer;
use App\Service\ShortenedUrlService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/shortened-url")
 */
class ShortenedUrlController extends BaseApiController
{
    /** @var FormErrorSerializer */
    private $errorNormalizer;

    /** @var ShortenedUrlService */
    private $shortenedUrlService;

    /**
     * @param FormErrorSerializer $errorNormalizer
     * @param ShortenedUrlService $shortenedUrlService
     */
    public function __construct(
        FormErrorSerializer $errorNormalizer,
        ShortenedUrlService $shortenedUrlService
    ) {
        $this->errorNormalizer = $errorNormalizer;
        $this->shortenedUrlService = $shortenedUrlService;
    }

    /**
     * @Route("/create", name="create_shortned_url", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $requestData = json_decode($request->getContent(), true);
        $form = $this->createForm(ShortenedUrlType::class);
        $form->submit($requestData);
        if (false === $form->isValid()) {
            return $this->buildErrorResponse(
                Response::HTTP_BAD_REQUEST,
                self::FAILED_VALIDATION,
                $this->errorNormalizer->convertFormToArray($form)
            );
        }

        $url = $this->shortenedUrlService->createShortenedUrl($form->getData(), $this->getUser());

        return $this->buildSuccessResponse(
            Response::HTTP_CREATED,
            $url->toArray()
        );
    }

    /**
     * @Route("/list", name="list_shortned_url", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $urls = $this->shortenedUrlService->listUrlsByUser($this->getUser());

        return $this->buildSuccessResponse(Response::HTTP_OK, $urls);
    }

    /**
     * @Route("/delete/{id}", name="delete_shortned_url", methods={"DELETE"})
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            $this->shortenedUrlService->deleteUrl($this->getUser(), $id);
        } catch (\Exception $exception) {
            return $this->buildErrorResponse(Response::HTTP_BAD_REQUEST, self::BAD_PARAMETERS_MESSAGE);
        }

        return $this->buildSuccessResponse(Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/edit/{id}", name="edit_shortned_url", methods={"PATCH"})
     *
     * @param int $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function edit(int $id, Request $request): JsonResponse
    {
        try {
            $requestData = \json_decode($request->getContent(), true);
            $form = $this->createForm(ShortenedUrlType::class);
            $form->submit($requestData);
            if (false === $form->isValid()) {
                return $this->buildErrorResponse(
                    Response::HTTP_BAD_REQUEST,
                    self::FAILED_VALIDATION,
                    $this->errorNormalizer->convertFormToArray($form)
                );
            }
            $url = $this->shortenedUrlService->editUrl($this->getUser(), $form->getData(), $id);
        } catch (\Exception $exception) {
            return $this->buildErrorResponse(
                Response::HTTP_BAD_REQUEST,
                self::BAD_PARAMETERS_MESSAGE
            );
        }

        return $this->buildSuccessResponse(Response::HTTP_OK, [$url->toArray(),]);
    }
}