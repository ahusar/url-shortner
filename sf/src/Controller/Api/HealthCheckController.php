<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HealthCheckController extends AbstractController
{
    /**
     * @Route("/ping", name="healthcheck", methods={"GET"})
     */
    public function index()
    {
        return $this->json('pong');
    }
}
