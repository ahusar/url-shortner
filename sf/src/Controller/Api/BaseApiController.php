<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseApiController extends AbstractController
{
    public const FAILED_VALIDATION = "Validation failed, please check input.";
    public const BAD_PARAMETERS_MESSAGE = "Bad request, please check parameters.";

    /**
     * @param int $statusCode
     * @param string|null $message
     * @param array $errors
     *
     * @return JsonResponse
     */
    protected function buildErrorResponse(int $statusCode, string $message = null, array $errors = []): JsonResponse
    {
        $data['status'] = $statusCode;
        if ($errors) {
            $data['errors'] = $errors;
        }

        if ($message) {
            $data['message'] = $message;
        }

        return $this->json($data, $statusCode);
    }

    /**
     * @param int $statusCode
     * @param null $data
     *
     * @return JsonResponse
     */
    protected function buildSuccessResponse(int $statusCode, $data = null): JsonResponse
    {
        $data = array_merge(
            ['status' => $statusCode],
            ['data' => $data]
        );

        return $this->json($data, $statusCode);
    }
}