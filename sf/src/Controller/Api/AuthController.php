<?php

namespace App\Controller\Api;

use App\Form\UserRegistrationFormType;
use App\Serializer\FormErrorSerializer;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/auth")
 */
class AuthController extends BaseApiController
{
    /** @var FormErrorSerializer */
    private $errorNormalizer;

    /** @var UserService */
    private $userService;

    /**
     * @param FormErrorSerializer $errorNormalizer
     * @param UserService $userService
     */
    public function __construct(
        FormErrorSerializer $errorNormalizer,
        UserService $userService
    ) {
        $this->errorNormalizer = $errorNormalizer;
        $this->userService = $userService;
    }

    /**
     * @Route("/login", name="auth_login", methods={"POST"})
     */
    public function login(Request $request)
    {
    }

    /**
     * @Route ("/register", name="auth_register", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse|RedirectResponse
     */
    public function register(Request $request)
    {
        $data = \json_decode($request->getContent(), true);
        $form = $this->createForm(UserRegistrationFormType::class);
        $form->submit($data);
        if (false === $form->isValid()) {
            return $this->buildErrorResponse(
                Response::HTTP_BAD_REQUEST,
                self::FAILED_VALIDATION,
                $this->errorNormalizer->convertFormToArray($form)
            );
        }

        try {
            $this->userService->createUser($data);
        } catch (\Exception $ex) {
            return $this->buildErrorResponse(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $ex->getMessage()
            );
        }

        return $this->redirectToRoute('auth_login', [
            'email' => $data['email'],
            'password' => $data['password'],
        ], Response::HTTP_TEMPORARY_REDIRECT);
    }
}