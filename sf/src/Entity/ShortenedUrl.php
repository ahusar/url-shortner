<?php

namespace App\Entity;

use App\Repository\ShortenedUrlRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=ShortenedUrlRepository::class)
 */
class ShortenedUrl
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sourceUrl;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $token;

    /**
     * @ORM\Column(type="integer", options={"default": "0"})
     */
    private $usages;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="shortnedUrls")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSourceUrl(): ?string
    {
        return $this->sourceUrl;
    }

    public function setSourceUrl(string $sourceUrl): self
    {
        $this->sourceUrl = $sourceUrl;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getUsages(): ?int
    {
        return $this->usages;
    }

    public function setUsages(int $usages): self
    {
        $this->usages = $usages;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|UserInterface $user
     *
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'token' => $this->getToken(),
            'sourceUrl' => $this->getSourceUrl(),
            'usages' => $this->getUsages(),
        ];
    }
}
