<?php

namespace App\Serializer;

use Symfony\Component\Form\FormInterface;

class FormErrorSerializer
{
    /**
     * @param FormInterface $data
     *
     * @return array
     */
    public function convertFormToArray(FormInterface $data): array
    {
        $errors = [];
        foreach ($data->getErrors(true) as $error) {
            $errors[][] = ['message' => $error->getMessage(), 'field' => $error->getOrigin()->getName()];
        }

        return $errors;
    }
}