<?php

namespace App\Encoders;

class ShortMd5Generator implements TokenGeneratorInterface
{
    /**
     * @return string
     */
    public function generateToken(): string
    {
        return substr(md5(mt_rand()), 0, 7);
    }
}