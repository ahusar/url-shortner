<?php

namespace App\Encoders;

interface TokenGeneratorInterface
{
    /**
     * @return string
     */
    public function generateToken(): string;
}