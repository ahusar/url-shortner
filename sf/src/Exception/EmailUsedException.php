<?php

namespace App\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

class EmailUsedException extends Exception
{
    protected $message = "Email already used.";
}