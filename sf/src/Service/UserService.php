<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\EmailUsedException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param array $userData
     *
     * @return User
     *
     * @throws EmailUsedException
     */
    public function createUser(array $userData)
    {
        $user = new User();
        $user->setEmail($userData['email']);
        if ($this->entityManager->getRepository(User::class)->findOneBy(['email' => $user->getEmail()])) {
            throw new EmailUsedException();
        }
        $user->setRoles([User::ROLE_USER]);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $userData['password']));
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}