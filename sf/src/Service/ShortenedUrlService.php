<?php

namespace App\Service;

use App\Encoders\TokenGeneratorInterface;
use App\Entity\ShortenedUrl;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

class ShortenedUrlService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TokenGeneratorInterface */
    private $generator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param TokenGeneratorInterface $generator
     */
    public function __construct(EntityManagerInterface $entityManager, TokenGeneratorInterface $generator)
    {
        $this->entityManager = $entityManager;
        $this->generator = $generator;
    }

    /**
     * @param array $data
     * @param UserInterface $user
     *
     * @return ShortenedUrl
     */
    public function createShortenedUrl(array $data, UserInterface $user): ShortenedUrl
    {
        $url = new ShortenedUrl();
        $url->setSourceUrl($data['sourceUrl']);
        $url->setUser($user);
        $url->setToken($this->generator->generateToken());
        $url->setUsages(0);
        $this->entityManager->persist($url);
        $this->entityManager->flush();

        return $url;
    }

    /**
     * @param UserInterface $user
     *
     * @return array
     */
    public function listUrlsByUser(UserInterface $user): array
    {
        /** @var ShortenedUrl[] $urls */
        $urls = $user->getShortenedUrls();
        $data = [];
        foreach ($urls as $key => $shortenedUrl) {
            $data[$key] = $shortenedUrl->toArray();
        }

        return $data;
    }

    /**
     * @param UserInterface $user
     * @param int $urlId
     *
     * @return bool
     * @throws EntityNotFoundException
     */
    public function deleteUrl(UserInterface $user, int $urlId): bool
    {
        $url = $this->fetchUrlByUserAndId($user, $urlId);
        $this->entityManager->remove($url);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @param UserInterface $user
     * @param array $data
     * @param int $id
     *
     * @return ShortenedUrl
     * @throws EntityNotFoundException
     */
    public function editUrl(UserInterface $user, array $data, int $id): ShortenedUrl
    {
        $url = $this->fetchUrlByUserAndId($user, $id);
        $sourceUrl = $data['sourceUrl'];
        $url->setSourceUrl($sourceUrl);
        $this->entityManager->flush();

        return $url;
    }

    /**
     * @param string $token
     *
     * @return string
     * @throws EntityNotFoundException
     */
    public function getAndVisitUrl(string $token): string
    {
        $url = $this->fetchUrlByToken($token);
        $url->setUsages($url->getUsages() + 1);
        $this->entityManager->flush();

        return $url->getSourceUrl();
    }

    /**
     * @param UserInterface $user
     * @param int $id
     *
     * @return ShortenedUrl
     * @throws EntityNotFoundException
     */
    private function fetchUrlByUserAndId(UserInterface $user, int $id): ShortenedUrl
    {
        return $this->fetchUrlByParameters(['id' => $id, 'user' => $user]);
    }

    /**
     * @param $token
     *
     * @return ShortenedUrl
     * @throws EntityNotFoundException
     */
    private function fetchUrlByToken($token): ShortenedUrl
    {
        return $this->fetchUrlByParameters(['token' => $token]);
    }

    /**
     * @param array $params
     *
     * @return ShortenedUrl
     * @throws EntityNotFoundException
     */
    private function fetchUrlByParameters(array $params): ShortenedUrl
    {
        $url = $this->entityManager->getRepository(ShortenedUrl::class)->findOneBy($params);
        if (!$url) {
            throw new EntityNotFoundException();
        }

        return $url;
    }
}